'use strict';

angular.module('tApp', [])
  .controller('ClipController', ['$scope', '$http', function($scope, $http) {
    $http.get('http://localhost:3000/clips').success(function(data) {
      $scope.clips = data;
    });
    
    $scope.addClip = function() {
      $http.post('http://localhost:3000/clips', {
        title: $scope.title,
        imgUrl: $scope.imgUrl,
        linkUrl: $scope.link
      }).success(function(data) {
        console.log(data);
        $scope.clips.push(data);
      });
    };
  }]);
