(function() {

  'use strict';

  var querySelector = document.querySelector.bind(document);

  var btnCancel = querySelector('.btn-cancel');
  var btnSubmit = querySelector('.btn-submit');

  var btnAddClip = querySelector('.icon-add-clip');
  var dialogClip = querySelector('.addClipDialog');
  var listView = querySelector('.list-view');


  var openDialog = function() {
    dialogClip.classList.remove('hidden');
    listView.classList.add('blur');
  };

  var closeDialog = function() {
    dialogClip.classList.add('hidden');
    listView.classList.remove('blur');
  };

  btnAddClip.addEventListener('click', openDialog);
  btnCancel.addEventListener('click', closeDialog);
  btnSubmit.addEventListener('click', closeDialog);
})();
